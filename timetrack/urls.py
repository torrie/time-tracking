from django.conf.urls import patterns, include, url
from django.contrib import admin

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'timetrack.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^ooadmin/', include(admin.site.urls)),
    url(r'^$', 'main.views.adder'),
    url(r'^login/', 'main.views.login'),
    url(r'^logout/', 'main.views.logout'),
)
