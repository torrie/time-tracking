import base64
import hashlib
import hmac
import json
from email.utils import formatdate
from datetime import datetime

from urllib import parse, request, error

__all__ = ['Client', ]


class MegaplanRequest(object):
    def __init__(self, hostname, access_id, secret_key, uri, data=None):
        self.method = 'POST'
        self.proto = 'https'
        self.uri = hostname + '/' + uri
        self.access_id = access_id
        self.secret_key = secret_key
        self.content_type = 'application/x-www-form-urlencoded'
        self.date = formatdate(float(datetime.now().strftime("%s")))
        self.signature = None
        self.data = data

    def sign(self):
        text = '\n'.join([self.method, '', self.content_type, self.date, self.uri])
        signature = hmac.new(self.secret_key.encode('utf-8'), text.encode('utf-8'), hashlib.sha1).hexdigest()
        self.signature = base64.b64encode(signature.encode('utf-8')).decode('utf-8')

    def send(self):
        url = self.proto + '://' + self.uri
        data = self.data and parse.urlencode(self.data).encode('utf-8')

        headers = {
            'Date': self.date.encode('utf-8'),
            'Accept': 'application/json',
            'User-Agent': 'python3-megaplan',
            'Content-Type': self.content_type,
        }
        if self.signature:
            headers['X-Authorization'] = (self.access_id + ':' + self.signature).encode('utf-8')

        req = request.Request(url, headers=headers, data=data)
        res = request.urlopen(req)
        res = res.read()
        data = json.loads(res.decode())
        if data['status']['code'] != 'ok':
            m = data["status"]["message"]
            if isinstance(m, list):
                m = m[0]

            if isinstance(m, dict):
                raise Exception(m["msg"])

            raise Exception(data['status']['message'])

        if "data" in data:
            return data['data']


class Client(object):
    def __init__(self, hostname, access_id=None, secret_key=None, staff_id=None):
        self.hostname = hostname
        self.access_id = access_id
        self.secret_key = secret_key
        self.staff_id = staff_id

    def get_staff_id(self):
        return self.staff_id

    def authenticate(self, login, password):
        """Authenticates the client.

        The access_id and secret_key values are returned.  They can be stored
        and used later to create Client instances that don't need to log in."""
        data = self.request('BumsCommonApiV01/User/authorize.api', {
            'Login': login, 'Password': hashlib.md5(password.encode()).hexdigest()
        }, signed=False)
        self.access_id = data['AccessId']
        self.secret_key = data['SecretKey']
        self.staff_id = data['EmployeeId']
        return self.access_id, self.secret_key

    def request(self, uri, args=None, signed=True):
        """Sends a request, returns the data.

        Args should be a dictionary or None; uri must not begin with a slash
        (e.g., "BumsTaskApiV01/Task/list.api").  If an error happens, an
        exception occurs."""
        if uri != "BumsCommonApiV01/User/authorize.api" and (self.access_id is None or self.secret_key is None):
            raise Exception('Authenticate first.')
        req = MegaplanRequest(str(self.hostname), str(self.access_id), str(self.secret_key), uri, args)
        if signed:
            req.sign()
        return req.send()

    # SOME HELPER METHODS

    def add_task(self, name, text, responsible=None, parent_task=None):
        args = {
            "Model[Name]": name,
            "Model[Statement]": text,
        }

        if responsible:
            args["Model[Responsible]"] = responsible

        if parent_task:
            args["Model[SuperTask]"] = parent_task

        return self.request("BumsTaskApiV01/Task/create.api", args)

    def get_incoming_tasks(self):
        """Returns all visible tasks"""
        return self.request('BumsTaskApiV01/Task/list.api', {
            'Folder': 'incoming',
            'Detailed': True,
        })["tasks"]

    def get_projects(self, folder="incoming", status="any"):
        return self.request("BumsProjectApiV01/Project/list.api", {
            "Folder": folder,
            "Status": status,
            "Detailed": True,
        })["projects"]

    def get_actual_tasks(self):
        """Returns your active tasks as a list of dictionaries."""
        return self.request('BumsTaskApiV01/Task/list.api', { 'Status': 'actual' })['tasks']

    def get_tasks_by_status(self, status=None):
        """Returns your active tasks as a list of dictionaries."""
        return self.request('BumsTaskApiV01/Task/list.api', { 'Status': status })['tasks']

    def get_staff_card(self):
        a = self.request('BumsStaffApiV01/Employee/card.api', {'Id': self.staff_id})
        return a['employee']['FirstName'] + ' ' + a['employee']['LastName']

    def get_task_details(self, task_id):
        """Returns task description or None if there's no such task."""
        try:
            return self.request('BumsTaskApiV01/Task/card.api', {'Id': self._task_id(task_id)})
        except error.HTTPError as e:
            if e.getcode() == 404:
                return None
            raise e

    def act(self, task_id, action):
        try:
            return self.request("BumsTaskApiV01/Task/action.api", {
                "Id": self._task_id(task_id),
                "Action": action,
            })
        except error.HTTPError as e:
            if e.getcode() == 403:
                return False
            raise e

    def get_all_comments(self, actual=False):
        return self.request('BumsCommonApiV01/Comment/all.api', {
            "OnlyActual": "true" if actual else "false",
        })["comments"]

    def get_task_comments(self, task_id):
        return self.request('BumsCommonApiV01/Comment/list.api', {
            'SubjectType': 'task',
            'SubjectId': task_id,
        })["comments"]

    def get_project_comments(self, project_id):
        return self.request('BumsCommonApiV01/Comment/list.api', {
            'SubjectType': 'project',
            'SubjectId': project_id,
        })["comments"]

    def add_task_comment(self, task_id, text, hours=0, date=None):
        return self._add_comment("task", task_id, text, hours, date)

    def add_project_comment(self, project_id, text, hours=0, date=None):
        return self._add_comment("project", project_id, text, hours, date)

    def _add_comment(self, _type, _id, text, hours, date=None):
        msg = {
            "SubjectType": _type,
            "SubjectId": self._task_id(_id),
            "Model[Text]": text,
            "Model[Work]": hours,
        }
        if date is not None:
            msg["Model[WorkDate]"] = date
        return self.request("BumsCommonApiV01/Comment/create.api", msg)

    def _task_id(self, task_id):
        if task_id < 1000000:
            task_id += 1000000
        return task_id

    def __repr__(self):
        if self.access_id is not None:
            return "<megaplan.Client access_id=%s>" % self.access_id
        return super(Client, self).__repr__()

from django.shortcuts import render
from main.models import *
from django.http import HttpResponse, HttpResponseRedirect
from django.core.exceptions import ObjectDoesNotExist
from django.template.loader import render_to_string
from django.core.context_processors import csrf
from django.views.decorators.csrf import csrf_exempt
from datetime import *
import random
import string


def random_string(length):
    pool = string.ascii_letters + string.digits
    return ''.join(random.choice(pool) for i in range(length))

# def main(request):
    
#     return HttpResponse(render_to_string('1.html', {'cat': items, 'cats':cats, 'people':people, 'gazel':gazel}))
@csrf_exempt
def adder(request):
    if request.method == 'POST':
        if 'uhsh' in request.COOKIES:
            hash_cookie = request.COOKIES.get('uhsh')
            try:
                user = User.objects.get(hash_cookie=hash_cookie)
                # надо взять то, что он прислал и забить в базу
                try:
                    task = request.POST['task']
                    minutes = int(request.POST['minutes'])
                    project = int(request.POST['project'])
                except:
                    return HttpResponse('0Введены некорректные данные.')
            except ObjectDoesNotExist:
                hr = HttpResponseRedirect('/login/')
                hr.delete_cookie('uhsh')
                return hr
            try:
                proj = Project.objects.get(id=project)
                
                th = Thing(name=task, project=proj, minutes=minutes, user=user)
                th.save()
                history = Thing.objects.filter(user=user).order_by('-id')[:10]

                context = {
                        'history': history,
                        'user': user,
                    }
                return HttpResponse(render_to_string('table.html', context))
            except:
                return HttpResponse('0Такого проекта не существует.')
            
        else:
            return HttpResponseRedirect('/login/')
    else:
        if 'uhsh' in request.COOKIES:
            hash_cookie = request.COOKIES.get('uhsh')
            try:
                user = User.objects.get(hash_cookie=hash_cookie)
            except ObjectDoesNotExist:
                hr = HttpResponseRedirect('/login/')
                hr.delete_cookie('uhsh')
                return hr
            history = Thing.objects.filter(user=user).order_by('-id')[:10]
            projects = Project.objects.all()
            context = {
                    'history': history,
                    'user': user,
                    'projects': projects,
                }
            return HttpResponse(render_to_string('index.html', context))
        else:
            return HttpResponseRedirect('/login/')


@csrf_exempt
def login(request):
    if request.method == 'POST':
        if 'uhsh' in request.COOKIES:
            # похож на залогиневшегося..
            hash_cookie = request.COOKIES.get('uhsh')
            try:
                user = User.objects.get(hash_cookie=hash_cookie)
                return HttpResponseRedirect('/')
                # да, залогинен
            except:
                # что-то прислал, но кука неверная
                # пытаемся авторизовать
                try:
                    you = Client(hostname='direktline.megaplan.ru')
                    you.authenticate(request.POST['login'], request.POST['password'])
                except:
                    context = {
                        'message': 'Авторизация не удалась',
                    }
                    return HttpResponse(render_to_string('login.html', context))
                try:
                    mm = User.objects.get(mp_id=int(you.get_staff_id()))
                    hr = HttpResponseRedirect('/')
                    hr.set_cookie('uhsh', mm.hash_cookie)
                    return hr
                except ObjectDoesNotExist:
                    hashy = random_string(64)
                    m = User(name=you.get_staff_card(), hash_cookie=hashy, mp_id=you.get_staff_id())
                    m.save()
                    hr = HttpResponseRedirect('/')
                    hr.set_cookie('uhsh', hashy)
                    return hr
        else:
            # что-то прислал, нет куки
            # пытаемся авторизовать
            try:
                you = Client(hostname='direktline.megaplan.ru')
                you.authenticate(request.POST['login'], request.POST['password'])
            except:
                context = {
                    'message': 'Авторизация не удалась',
                }
                return HttpResponse(render_to_string('login.html', context))
            try:
                mm = User.objects.get(mp_id=int(you.get_staff_id()))
                hr = HttpResponseRedirect('/')
                hr.set_cookie('uhsh', mm.hash_cookie)
                return hr
            except ObjectDoesNotExist:
                hashy = random_string(64)
                m = User(name=you.get_staff_card(), hash_cookie=hashy, mp_id=you.get_staff_id())
                m.save()
                hr = HttpResponseRedirect('/')
                hr.set_cookie('uhsh', hashy)
                return hr
    else:
        # отдаем просто форму для авторизации
        context = {}
        return HttpResponse(render_to_string('login.html', context))

def logout(request):
    hr = HttpResponseRedirect('/login/')
    hr.delete_cookie('uhsh')
    return hr