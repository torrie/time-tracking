# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='thing',
            name='date',
            field=models.DateField(default=datetime.datetime(2015, 6, 30, 5, 54, 56, 256899), verbose_name='Дата'),
        ),
        migrations.AlterField(
            model_name='thing',
            name='minutes',
            field=models.IntegerField(verbose_name='Минут', validators=[django.core.validators.MinValueValidator(1), django.core.validators.MaxValueValidator(480)]),
        ),
    ]
