# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Project',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, verbose_name='ID', auto_created=True)),
                ('name', models.CharField(verbose_name='Название проекта', max_length=255)),
            ],
            options={
                'verbose_name_plural': 'Проекты',
                'verbose_name': 'Проект',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Thing',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, verbose_name='ID', auto_created=True)),
                ('name', models.CharField(verbose_name='Что делалось', max_length=255)),
                ('date', models.DateField(default=datetime.datetime(2015, 6, 29, 13, 9, 36, 365494), verbose_name='Дата')),
                ('minutes', models.IntegerField(verbose_name='Минут')),
                ('project', models.ForeignKey(verbose_name='Проект', to='main.Project')),
            ],
            options={
                'verbose_name_plural': 'Работы',
                'verbose_name': 'Работа',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='User',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, verbose_name='ID', auto_created=True)),
                ('name', models.CharField(verbose_name='Имя', max_length=255)),
                ('hash_cookie', models.CharField(verbose_name='Hash', max_length=255)),
                ('mp_id', models.IntegerField(verbose_name='ID в мегаплане')),
            ],
            options={
                'verbose_name_plural': 'Пользователи',
                'verbose_name': 'Пользователь',
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='thing',
            name='user',
            field=models.ForeignKey(verbose_name='Пользователь', to='main.User'),
            preserve_default=True,
        ),
    ]
