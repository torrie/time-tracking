# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0002_auto_20150630_0554'),
    ]

    operations = [
        migrations.AlterField(
            model_name='thing',
            name='date',
            field=models.DateField(verbose_name='Дата', default=datetime.datetime(2015, 6, 30, 5, 55, 2, 227853)),
        ),
    ]
