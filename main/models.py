from django.db import models
from datetime import *
from django.core.validators import MinValueValidator, MaxValueValidator


class Project(models.Model):
    name = models.CharField(max_length=255, verbose_name='Название проекта')
    
    class Meta:
        verbose_name = "Проект"
        verbose_name_plural = "Проекты"

    def __str__(self):
        return 'Проект %s' % self.name


class User(models.Model):
    name = models.CharField(max_length=255, verbose_name='Имя')
    hash_cookie = models.CharField(max_length=255, verbose_name='Hash')
    mp_id = models.IntegerField(verbose_name='ID в мегаплане')
    
    class Meta:
        verbose_name = "Пользователь"
        verbose_name_plural = "Пользователи"

    def __str__(self):
        return 'Пользователь %s' % self.name


# Create your models here.
class Thing(models.Model):
    name = models.CharField(max_length=255, verbose_name='Что делалось')
    project = models.ForeignKey(Project, verbose_name='Проект')
    user = models.ForeignKey(User, verbose_name='Пользователь')
    date = models.DateField(default=datetime.now(), verbose_name='Дата', help_text='')
    
    minutes = models.IntegerField(verbose_name='Минут', validators=[MinValueValidator(1),
                                       MaxValueValidator(480)])

    class Meta:
        verbose_name = "Работа"
        verbose_name_plural = "Работы"

    # def get_absolute_url(self):
    #     return "/c-%s/%s/" % (self.cat.slug, self.slug)

    def __str__(self):
        return '%s' % (self.name)

    def opa(self):
        hours = self.minutes // 60
        minutes = self.minutes - hours*60
        return '%dч%02dм' % (hours, minutes)
