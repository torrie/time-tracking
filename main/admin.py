from django.contrib import admin
from main.models import *


class UserAdmin(admin.ModelAdmin):
    list_display = ('name', 'mp_id')


class ThingAdmin(admin.ModelAdmin):
    list_display = ('name', 'project', 'user', 'minutes', 'date')


class ProjectAdmin(admin.ModelAdmin):
    list_display = ('id','name')




admin.site.register(User, UserAdmin)
admin.site.register(Thing, ThingAdmin)
admin.site.register(Project, ProjectAdmin)

